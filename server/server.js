/**
*App.js server side
*/

// Importing express and defining server
const express = require("express");
const server = express();

//Importing body-parser
const bodyParser = require("body-parser");

//Importing moment.js. Source: https://momentjs.com/docs/
const moment = require("moment");
moment().format();

const NODE_PORT = process.env.PORT || 4000;

server.use(express.static(__dirname + "/../client/"));
server.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
server.use(bodyParser.json({ limit: '50mb' }));

let forms = []; //initialise empty forms

// Methods

server.get(
    "/api",
    (req, res) => {
        console.log("200: GET /api/");
        console.log("c. Submitted data should be visible on the Express console");
        console.log(req.body);
        res.status(200).json(forms);
    }
);

server.post(
    "/api/submit",
    (req, res) => {
        const form = req.body;
        console.log("200: POST /api/submit");
        console.log("3a. Sent to the server (via GET or POST)");
        console.log(form);
        forms.push(form);
    }
);

// Listener
server.listen(
    NODE_PORT,
    () => {
        console.log(`Listening with server/server.js at port ${NODE_PORT}`);
    }
);