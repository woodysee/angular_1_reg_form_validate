# Angular 1.x Registration Form with validation #

This is an Angular 1.x user registration form with validation.

A landing page with a user registration form

## Concepts applied: ##

1. HTML / CSS
2. Bootstrap (optional)
3. Basics of AngularJS
    - ng module/controller
    - ng click/show/hide/if (optional)
    - ng model/bind
    - ng repeat (optional)
    - ng form
    - User-defined Directives (optional)
    - $http
4. Basics of Express
    - app.get or post route

## Features ##
1. Mandatory form fields (with HTML5 input types)
    - a. Email (email)
    - b. Password (password)
    - c. Name (text)
    - d. Gender (radio)
    - e. Date of Birth (date)
    - f. Address (text)
    - g. Country (text or select)
    - h. Contact Number (text)
    - i. Submit (submit or button)
2. The following field validations are expected (you may decide for yourself how to
feedback validation error messages to the user)
    - a. Mandatory fields cannot be left blank
    - b. Email entered must be a valid email address
    - c. Password must adhere to the following strong password policies:
        - i. have at least 8 characters
        - ii. use both uppercase and lowercase letters (case sensitivity)
        - iii. include one or more numerical digits
        - iv. include one of these special characters @, #, $
    - d. Registrant must be at least 18 years old
    - e. Contact number should not contain invalid characters (+, -, space, and round brackets are allowed, as they are used for area code and extensions)

3. Information submitted by this registration form should be:
    - a. Sent to the server (via GET or POST)
    - b. Inform registrant of successful form submission by displaying either a Thank You page, or a Modal, or inline HTML message
    - c. Submitted data should be visible on the Express console

## DURATION ##

You have 6 hours (not inclusive of 1 hour lunch break) to complete this Assessment. Your
final commit must be pushed to your BitBucket or GitHub repo before 5.30pm .