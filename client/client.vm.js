"use strict";
(
    () => {
        angular.module("RegClient").controller("RegVM", RegVM);

        RegVM.$inject = ["$http"];

        function RegVM($http) {
            const vm = this;
            vm.submission = {
                email: "",
                password: "",
                name: "",
                gender: "",
                dob: "",
                address: "",
                country: "",
                contact: ""
            };
            vm.done = false;
            vm.regex = {
                password: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$])[A-Za-z\d@#$]+\W/,
                contact: /^((((\()|(\+)|(\+\()|(\\))|(\+\([0-9]\))|(\(\+[0-9]\)))+[0-9]+([^A-z\W])+(\)|\)|))([0-9-]+[^A-Za-z\W])|([0-9-]+[^A-Za-z\W])/
            };
            vm.countries = [
                {
                    name: "Singaporean",
                    value: "SGP"
                },
                {
                    name: "Indonesian",
                    value: "IDR"
                },
                {
                    name: "Thai",
                    value: "THA"
                },
                {
                    name: "Malaysian",
                    value: "MYA"
                }
            ];
            vm.onSubmit = () => {
                console.log("Form submitted to express server with this data:");
                console.log(vm.submission);
                console.log(vm.submission.email);
                console.log(vm.submission.password);
                console.log(vm.submission.name);
                console.log(vm.submission.gender);
                console.log(vm.submission.dob);
                console.log(vm.submission.address);
                console.log(vm.submission.country);
                console.log(vm.submission.contact);
                $http.post("/api/submit", vm.submission).then(
                    (res) => {
                        console.log(res);
                    }
                ).catch(
                    (error) => {
                        console.log(error);
                    }
                );
                vm.done = true;
                    vm.submission = { email: "", password: "", name: "", gender: "", dob: "", address: "", country: "", contact: "" };
            };

            vm.isAKid = (dateStr) => {
                // console.log("2d. Registrant must be at least 18 years old - ", dateStr);
                const ageLimit = 18;
                const getAge = (dateString) => {
                    //Take today's date
                    const today = new Date();
                    //Compare it with the date of birth
                    const birthday = new Date(dateString);
                    // Age of dob
                    let age = today.getFullYear() - birthday.getFullYear();
                    const m = today.getMonth() - birthday.getMonth();
                    if (m < 0 || (m === 0 && today.getDate() < birthday.getDate())) {
                        age--;
                    }
                    // console.log(`Age of submitter is ${age}`);
                    return age;
                };
                //If below eightteen, return false
                const usersAge = getAge(dateStr);
                // console.log(`usersAge`, usersAge);
                // console.log(`usersAge <= ageLimit is ${usersAge <= ageLimit}`);
                return usersAge < ageLimit;
            };

            //On load invokes the form
            (
                () => {
                    vm.submission.country = "";
                }
            )();
        }
    }
)();
